﻿<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Smart Seals</title>
    <!-- Animations -->
    <link href="/css/animate.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/alertify.css">
    <link rel="stylesheet" href="/css/alertify.min.css">

    <link rel="stylesheet" href="/Bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
   
</head>

<body>


    <a href="https://api.whatsapp.com/send?phone=+573006670059&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n" class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>


    <div  class="container-fluid ">
        
           

					   <!-- Carousel ================================================== -->
   <section class="carousel slide carousel-fade" data-ride="carousel" id="carouselhome">
    

        <div class="carousel-inner">

            <div class="item slide01 active" >
			
			   <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"><img src="img/logo-smart.png" width="150px" alt="" srcset=""></a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav pull-right">
                                    
                                    <li><a href="#servicios" class="ancla">SERVICIOS</a></li>
                                    <li><a href="#empresa" class="ancla">NUESTRA EMPRESA</a></li>
									<li><a href="/trabaje-nosotros.php" class="ancla">TRABAJA CON NOSOTROS</a></li>
                                    <li><a href="#contacto" class="ancla">CONTÁCTANOS</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                            <!-- /.container-fluid -->
                        </nav>
					
               <article>
                     <div class="wow bounceInDown textslides">
                            <div class="col-md-7"></div>
                            <div class="col-md-5 text-justify ">
                                <h1 class="head-h1">Smart Tracking <br class="hidesm"> System</h1>
                                <p>
                                    Ofrecemos servicios para la trazabilidad de activos en tiempo real para empresas del sector logístico, transporte público, minero entre otros. Nuestra herramienta Smart Tracking, permite la integración con mapas y herramientas corporativas de nuestros clientes manteniendo un control efectivo de las labores diarias.<br>
                                    <br>
                                    
                                    <br>

                                    <a class="btn-leer-mas btn" href="#tracking">LEER MÁS</a>

                                   
                                </p>
                            </div>
                        </div>
                                
               </article>              
            </div>


            
            <div class="item slide02" >
			
			   <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"><img src="img/logo-smart.png" width="150px" alt="" srcset=""></a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                <ul class="nav navbar-nav pull-right">
                                    
                                    <li><a href="#servicios" class="ancla">SERVICIOS</a></li>
                                    <li><a href="#empresa" class="ancla">NUESTRA EMPRESA</a></li>
									<li><a href="/trabaje-nosotros.php" class="ancla">TRABAJA CON NOSOTROS</a></li>
                                    <li><a href="#contacto" class="ancla">CONTÁCTANOS</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                            <!-- /.container-fluid -->
                        </nav>
					

               <article>
                      <div class="wow bounceInDown textslides">
                            <div class="col-md-7"></div>
                            <div class="col-md-5 text-justify">
                                <h1 class="head-h1">Smart  <br class="hidesm">Detect </h1>
                                <p>
                                    Nuestro sistema de detección y control de pérdidas Smart Detect, permite a las empresas del sector energético tener una gran flexibilidad y facilidad a la hora de disminuir sus pérdidas de media tensión <br>
                                    
                                    <br>

                                    <a class="btn-leer-mas btn" href="/trabaje-nosotros.php">LEER MÁS</a>

                                   
                                </p>
                            </div>
                        </div>
                                
               </article>              
            </div>

            <div class="item slide03" >
		
			   <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"><img src="img/logo-smart.png" width="150px" alt="" srcset=""></a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
                                <ul class="nav navbar-nav pull-right">
                                    
                                    <li><a href="#servicios" class="ancla">SERVICIOS</a></li>
                                    <li><a href="#empresa" class="ancla">NUESTRA EMPRESA</a></li>
									<li><a href="/trabaje-nosotros.php" class="ancla">TRABAJA CON NOSOTROS</a></li>
                                    <li><a href="#contacto" class="ancla">CONTÁCTANOS</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                            <!-- /.container-fluid -->
                        </nav>
               <article>
                      <div class="wow bounceInDown textslides">
                            <div class="col-md-7"></div>
                            <div class="col-md-5 text-justify">
                                <h1 class="head-h1">Smart Metering <br class="hidesm"> System</h1>
                                <p>
                                    SMART SEALS CO es una organización con un enfoque 
                                    inovador en el iseño y desarrollo de soluciones personalizadas 
                                    de alta tecnología.Contamos con sistemas de medición avanzada para el control de pérdidas de energía de las compañías eléctricas del país.  Comprendiendo la necesidad de tener un sistema de medida que armonice un equipo de medición robusto.
                                    <br>

                                    <!--<a class="btn-leer-mas btn" href="/pdf/1-brochure-smart-seals.pdf">DESCARGAR PORTAFOLIO</a>-->

                                   
                                </p>
                            </div>
                        </div>
                                
               </article>              
            </div>




            

             <!-- Controls -->
       
        </div>

    </section>
    <!-- /.carousel -->

                
          
    
        <a id="servicios"></a>
        <div class="container">
            <div class="">
                <div class="col-md-12 servicio-pad text-center">
             
                    <h2>Productos y Servicios</h2>
                    <p>
                        Nuestra gama de productos industriales brinda flexibilidades tecnológicas para satisfacer las necesidades puntales
                        <br> de nuestros clientes, ofreciendo soluciones en el ámbito M2M,loT y canales de datos alta seguridad.
                    </p>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 servicio-pad servicio-right wow bounceInDown">
                        <img src="img/server.png" class="img img-responsive imagenes-server" alt="">
                        <h3>Servers</h3> Nuestros servers cuentan con variedad de interfaces de comunicación (Celular, Ethernet, RS232, Wifi, Bluetooth, etc) y con capacidad de implementación de protocolos puntuales de cada segmento, entre ellos: sistemas avanzados de monitoreo por video, bases de datos, sistemas avanzados de medición eléctrica (compatibles con protocolos DLMS/Cosem, PLC Prime, etc).
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4 servicio-pad servicio-right wow bounceInDown">
                        <img src="img/sellos-etiqetas.png" class="img img-responsive imagenes-server" alt="">
                        <h3>Sellos y etiquetas</h3> Nuestros sellos y etiquetas de seguridad son diseñados y fabricados con capacidades anti-tamper para dificultar la manipulación y alteración de activos críticos de las compañías. Su diseño flexible, permite a los usuarios adaptarlos de acuerdo con sus requerimientos.
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 servicio-right wow bounceInDown">
                        <img src="img/routers.png" class="img img-responsive imagenes-server" alt="">
                        <h3>Routers</h3> Routers industriales M2M con múltiples capacidades de comunicación inalámbricas que ofrecen una gran versatilidad para implementaciones donde la seguridad de los datos y multiplicidad de tecnologías de comunicación es una prioridad.  Nuestros routers cuentan con comunicación celular (4G/3G, dual sim), interfaces Ethernet, Serial RS232, Wifi y GPS.
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive hidden-sm hidden-xs" src="/img/icon-logo-smart.png" alt="Logo smart">
                    </div>
                    <div class="col-md-4 servicio-right wow bounceInDown" id="tracking">
                        <img src="img/Smart-T.png" class="img img-responsive imagenes-server" alt="">
                        <h3>Smart Traking</h3> Ofrecemos servicios para la trazabilidad de activos en tiempo real para empresas del sector logístico, transporte público, minero entre otros. 
                        <br> Nuestra herramienta Smart Tracking, permite la integración con mapas y herramientas corporativas de nuestros clientes manteniendo un control efectivo de las labores diarias.
                    </div>
                </div>
                <div class="row margin-bot">
                    <div class="col-md-4 servicio-right wow bounceInDown" id="detect">
                        <img src="img/Smart-d.png" class="img img-responsive imagenes-server" alt="">
                        <h3 class="text-h3">Smart Detect</h3> Nuestro sistema de detección y control de pérdidas Smart Detect, permite a las empresas del sector energético tener una gran flexibilidad y facilidad a la hora de disminuir sus pérdidas de media tensión, impactando un alto volumen de clientes, y permitiendo en tiempo real definir cuales clientes están incurriendo en defraudación de energía.
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4 servicio-right wow bounceInDown">
                        <img src="img/help.png" class="img img-responsive imagenes-server" alt="">
                        <h3>Asesorias</h3> Contamos con un equipo de expertos con más de 10 años de experiencia en recuperación de energía, sistemas de seguridad industrial, detección de fraude, telecomunicaciones. Ofrecemos metodologías, técnicas, equipos y herramientas de supervisión y control para una óptimo análisis, detección, implementación y control de las pérdidas de las compañías eléctricas del país.
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <a class="btn boton-brochure" target="_blank" href="/pdf/1-brochure-smart-seals.pdf" >BROCHURE</a>

                        
                    </div>
                </div>
            </div>

        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="empresa" class="row">
                        <div class="col-md-10 semi-circulo-2">

                            <div class="row content visionpp">
                                <div class="col-md-12">
                                  
                                    <h2 class="class-h2">Nuestra empresa y <br> nuestros valores</h2>
                                </div>
                                <div class="col-md-2">
                                    <h3 class="text-mision-v">Misi&oacute;n</h3>
                                </div>
                                <div class="col-md-10">

                                    <p class="text-p-small">
                                        SMART SEALS CO es una organización con un enfoque innovador
                                        en el diseño y desarrollo de soluciones personalizadas de alta
                                        tecnologia, que permitan resolver las problemáticas particulares de
                                        cada cliente, en los sectores público y privado.
                                    </p>
                                </div>

                                <div class="col-md-2">
                                    <h3 class="text-mision-v">Visi&oacute;n</h3>
                                </div>
                                <div class="col-md-10">
                                    <p class="text-p-small">
                                        SMART SEALS CO espera ser reconocida en los próximos cinco años
                                        como el aliado perfecto para sus clientes,con la capacidad de responder
                                        con soluciones innovadoras de alta tecnología a cualquier tipo de
                                    </p>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-11 ">
                               
									<a class="btn btn-ver-mas" href="/trabaje-nosotros.php" >TRABAJA CON NOSOTROS</a>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="data">
                                            <div class="col-md-12 padd wow bounceInDown">
                                                <div class="cuadros">
                                                    <h3>01</h3>
                                                    <h4>Honestidad</h4>
                                                    <p>
                                                        Es la conducta que presenta al ser humano que integra la
                                                        empresa, que tiene como principio básico de lealtad para
                                                        consigo mismo, con los demás y con los negocios que se desarrollen.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" data">


                                            <div class="col-md-12 padd wow bounceInDown">
                                                <div class="cuadros">
                                                    <h3>02</h3>
                                                    <h4>Investigación</h4>
                                                    <p>
                                                        Es la conducta que presenta al ser humano que integra la
                                                        empresa, que tiene como principio básico de lealtad para
                                                        consigo mismo, con los demás y con los negocios que se desarrollen.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 ">

                                        <div class="">

                                            <div class="col-md-12 padd wow bounceInDown">
                                                <div class="cuadros">
                                                    <h3>03</h3>
                                                    <h4>Investigación</h4>
                                                    <p>
                                                        Es la conducta que presenta al ser humano que integra la
                                                        empresa, que tiene como principio básico de lealtad para
                                                        consigo mismo, con los demás y con los negocios que se desarrollen.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 padd wow bounceInDown">
                                                <div class="cuadros">
                                                    <h3>04</h3>
                                                    <h4>Compromiso</h4>
                                                    <p>
                                                        Es la conducta que presenta al ser humano que integra la
                                                        empresa, que tiene como principio básico de lealtad para
                                                        consigo mismo, con los demás y con los negocios que se desarrollen.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 padd wow bounceInDown">
                                                <div class="cuadros">
                                                    <h3>05</h3>
                                                    <h4>Respeto</h4>
                                                    <p>
                                                        Es la conducta que presenta al ser humano que integra la
                                                        empresa, que tiene como principio básico de lealtad para
                                                        consigo mismo, con los demás y con los negocios que se desarrollen.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>






    <div class="container-fluid ">

        <div class="footerbot">


            <div id="contacto" class="container">
                <div class="row">


                    <div class="col-md-12">
                        <footer>
                            <div class="container">
                                <div class="col-md-12">
                                    <h2 class=" h2-f text-center">
                                        Completa el formulario<br>
                                        para contactarte
                                    </h2>
                                </div>
                                <div class="row foot text-center">
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <input type="text" class="form-control" id="nombretxt" placeholder="Nombre">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <input type="text" class="form-control" id="celulartxt" placeholder="Celular">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <input type="email" id="correotxt" class="form-control" placeholder="Correo">
                                            <div id="xmail" class="hide"><h6>Ingresa un email valido</h6></div>
                                        </div>
                                    </div>
                                    

									<div class="col-md-9">
									 <div class="form-group">
									 <textarea name="asuntotxt" id="asuntotxt" cols="40" rows="2" placeholder="Asunto" class="form-control"></textarea>
                                       
                                        </div>
									</div>
									<div class="col-md-3 text-center">
                                        <div class="form-group ">

                                            <button id="enviaremail" class="btn btn-enviar center">ENVIAR</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="row logo-foot">
                                    <div class="col-md-2">
                              
                                        <img src="img/logo-smart.png" class="log-ffot" alt="logo smart footer">
                                     
                                    </div>
                                    <br>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-7 text-right">
                                        <ul class="menu-foot">
                                        <li><a href="index.php" class="ancla">INICIO</a></li>
                                            <li><a href="index.php#servicios" class="ancla">SERVICIOS</a></li>
                                            <li><a href="index.php#empresa" class="ancla">NUESTRA EMPRESA</a></li>
                                            <li><a href="index.php#contacto" class="ancla">CONTACTANOS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </footer>


                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/Bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="/Bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/alertify.min.js"></script>
    <script src="/scripts/wow/wow.js"></script>
    <script src="/scripts/wow/device.min.js"></script>
    <script src="/js/stickyMojo.js"></script>
    <script src="/js/utilities.js"></script>
    <script src="/js/emailenvio.js"></script>
    <script src="/js/events.js"></script>

</body>

</html>