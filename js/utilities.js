﻿$(".onlynumbers").keydown(function (event) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
        // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
    }
});

$(".formatphone").focusout(function () {
    var regexObj = /^(?:\+?1[-. ]?)?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (regexObj.test($(this).val())) {
        $(this).val($(this).val().replace(regexObj, "($1) $2-$3"));
    }
});
var locationpage = window.location.pathname;
$("#mainmenu a").each(function () {
    var thisobject = $(this);
    //alert(thisobject.attr("href"));
    if (thisobject.attr("href") == locationpage || (thisobject.attr("href") == "./" && locationpage == "/")) {
        thisobject.addClass("active"); entro = true;
    }
});

//Animations
$(document).ready(function () {
    if ($('html').hasClass('desktop')) {
        new WOW().init();
    }

    $('.ancla').click(function () {
        var link = $(this);
        var anchor = link.attr('href');
        $('html, body').stop().animate({
            scrollTop: jQuery(anchor).offset().top
        }, 2000);
        return false;
    });
    $('.carousel').carousel({
        interval: 13000
    });

});