function scrollDetect() {
  var lastScroll = 0;
  document.getElementById("goUp").style.display = "none";
  window.onscroll = function () {
    let currentScroll =
      document.documentElement.scrollTop || document.body.scrollTop;
    if (!currentScroll) {
    }
    document.getElementById("goUp").style.display = "none";
    if (currentScroll > 1000) {
      document.getElementById("goUp").style.display = "block";
    }
    if (currentScroll <= 1000) {
      document.getElementById("goUp").style.display = "none";
    }
  };
}

scrollDetect();
